// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package playground;

import org.apache.log4j.*;

/** Hello world app with traces. */
public final class HelloWorld
{

  /** static logger class. */
  private static final Logger LOGGER = Logger.getLogger(HelloWorld.class);

  /** loop constant. */
  private static final int LOOP_VERY_OFTEN = 100000000;

  /** private CTOR, do not use. */
  private HelloWorld()
  {
  }

  /**
   * main entrypoint to logging sample applcation.
   * 
   * @param args
   *          unused.
   */
  public static void main(final String[] args)
  {
    // this main is just a sample for logging.
    LOGGER.debug("First java Sample\n(modified again)\n");
    BasicConfigurator.configure();
    LOGGER.debug("Hello world.");

    int number = 1;

    switch (number)
    {
      case 1:
        LOGGER.debug("fall through.");
        // releif pattern:
        // fall through by intention
      case 2:
        LOGGER.debug("1 or 2 detected.");
        break;
      case 3:
        LOGGER.debug("3 detected.");
        break;
      default:
        break;
    }
    measureLogTime2();
    LOGGER.info("From my First Java Sample.");
  }

  /** Sample function for tracing. */

  // i do want the printf here because it is a sample!
  @SuppressWarnings("PMD.SystemPrintln")
  public static void measureLogTime2()
  {
    Logger.getRootLogger().setLevel(Level.INFO);
    long start = System.currentTimeMillis();

    boolean lLogDebug = LOGGER.isDebugEnabled();
    if (lLogDebug)
    {
      for (long i = 0; i < LOOP_VERY_OFTEN; i++)
      {
        LOGGER.debug("DebugMessage");
      }
    }

    long end = System.currentTimeMillis();
    long elapsed = end - start;

    System.out.println("Execution time " + elapsed + " ms.");
  }
}
