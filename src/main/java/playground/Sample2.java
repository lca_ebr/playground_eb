// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package playground;

import javax.swing.JOptionPane;

/** Hello-world sample with GUI window. */
public final class Sample2
{
  /** private CTOR, do not use. */
  public Sample2()
  {
  }

  public static void showHelloWorldDialog()
  {
    // just show a message-box.
    String lHelloWorldMessage = "EB-Sample2";
    JOptionPane.showMessageDialog(null, lHelloWorldMessage);
  }

  /**
   * main entrypoint to sample app.
   * 
   * @param args
   *          unused.
   */
  public static void main(final String[] args)
  {
    // just show a message-box.
    showHelloWorldDialog();
  }

}
