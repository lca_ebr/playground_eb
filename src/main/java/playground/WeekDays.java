// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package playground;

import java.util.*;

class WeekDays
{
  private final transient Map<DaysOfWeek, Integer> weekDayMap;

  enum DaysOfWeek
  {
    montag, dienstag, mittwoch, donnerstag, freitag, samstag, sonntag
  }

  WeekDays()
  {
    weekDayMap = new HashMap<DaysOfWeek, Integer>();
    weekDayMap.put(DaysOfWeek.montag, 1);
    weekDayMap.put(DaysOfWeek.dienstag, 2);
    weekDayMap.put(DaysOfWeek.mittwoch, 3);
    weekDayMap.put(DaysOfWeek.donnerstag, 4);
    weekDayMap.put(DaysOfWeek.freitag, 5);
    weekDayMap.put(DaysOfWeek.samstag, 6);
    weekDayMap.put(DaysOfWeek.sonntag, 7);
  }

  public int wochenTagNummer(DaysOfWeek weekDay)
  {
    return weekDayMap.get(weekDay);
  }

}
