// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package playground;

import javax.swing.JFrame;
import org.apache.log4j.Logger;

public final class FrameWindow
{
  public static final int WINDOW_WIDTH = 600;
  public static final int WINDOW_HEIGHT = 400;
  /** static logger class. */
  private static final Logger LOGGER = Logger.getLogger(HelloWorld.class);

  /** Do not use. */
  private FrameWindow()
  {
  }

  /**
   * main entrypoint to framewindow sample.
   * 
   * @param args
   *          unused.
   */
  public static void main(String[] args)
  {
    // this main simply opens a message-box window.
    String lHelloWorldMessage = "EB-Sample3 will now open a window...";
    LOGGER.debug(lHelloWorldMessage);

    JFrame lFrameWindow = new JFrame();
    lFrameWindow.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    lFrameWindow.setTitle("EB Dobini");
    lFrameWindow.setVisible(true);
    LOGGER.debug("Window is now visible");
  }
}