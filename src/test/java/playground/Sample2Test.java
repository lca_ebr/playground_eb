// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package playground;

import javax.swing.JOptionPane;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(JOptionPane.class)
public class Sample2Test
{
  @Test
  public void testShowHelloWorldDialog()
  {
    // mock all static methods within JOptionPane
    PowerMockito.mockStatic(JOptionPane.class);

    // would normally show a message box two times
    Sample2.showHelloWorldDialog();
    Sample2.showHelloWorldDialog();

    // now verify that the showMessageDialog method
    // has been called two times with the parameter "EB-Sample2"
    PowerMockito.verifyStatic(Mockito.times(2));
    JOptionPane.showMessageDialog(null, "EB-Sample2");
  }

  @Test
  public void testMain()
  {
    PowerMockito.mockStatic(JOptionPane.class);
    new Sample2();
    String args[] = { "" };
    Sample2.main(args);
  }
}
