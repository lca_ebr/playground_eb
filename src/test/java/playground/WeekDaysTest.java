// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package playground;

import static org.junit.Assert.assertSame;
import org.junit.Test;
import playground.WeekDays.DaysOfWeek;

public class WeekDaysTest
{

  @Test
  public void testMonday()
  {
    WeekDays week = new WeekDays();
    assertSame("montag is wrong or missing", week.wochenTagNummer(DaysOfWeek.montag), 1);
  }

  @Test
  public void testTuesday()
  {
    WeekDays week = new WeekDays();
    assertSame("dienstag is wrong or missing", week.wochenTagNummer(DaysOfWeek.dienstag), 2);
  }

  @Test
  public void testWednesday()
  {
    WeekDays week = new WeekDays();
    assertSame("mittwoch is wrong or missing", week.wochenTagNummer(DaysOfWeek.mittwoch), 3);
  }

  @Test
  public void testThursday()
  {
    WeekDays week = new WeekDays();
    assertSame("donnerstag is wrong or missing", week.wochenTagNummer(DaysOfWeek.donnerstag), 4);
  }

  @Test
  public void testFriday()
  {
    WeekDays week = new WeekDays();
    assertSame("freitag is wrong or missing", week.wochenTagNummer(DaysOfWeek.freitag), 5);
  }

  @Test
  public void testSaturday()
  {
    WeekDays week = new WeekDays();
    assertSame("samstag is wrong or missing", week.wochenTagNummer(DaysOfWeek.samstag), 6);
  }

  @Test
  public void testSunday()
  {
    WeekDays week = new WeekDays();
    assertSame("sonntag is wrong or missing", week.wochenTagNummer(DaysOfWeek.sonntag), 7);
  }
}
